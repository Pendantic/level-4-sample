﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	public float sightDistance = 10;
	private PlayerMovement player;
	private NavMeshAgent agent;
	public Gun gun;
	public float chaseTime = 5f;
	private float chaseTimeLeft = 0f;
	// Use this for initialization
	void Start ()
	{
		player = FindObjectOfType<PlayerMovement> ();
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (player == null) {
			return;
		}

		if (Vector3.Distance (transform.position, player.transform.position) <= sightDistance) {
			RaycastHit hitInfo;
			if (Physics.Linecast (transform.position, player.transform.position, out hitInfo)) {
				if (hitInfo.collider.GetComponent<PlayerMovement> ()) {
					CanSeePlayer ();
				} else {
					chaseTimeLeft -= Time.deltaTime;
					if (chaseTimeLeft > 0) {
						agent.SetDestination (player.transform.position);
					} else {
						agent.SetDestination (transform.position);
					}

				}
			} else {
				CanSeePlayer ();
			}


		}
	}
	void CanSeePlayer ()
	{
		agent.SetDestination (player.transform.position);
		chaseTimeLeft = chaseTime;
		gun.transform.LookAt (player.transform);
		gun.Fire (gun.transform);
	}
}
