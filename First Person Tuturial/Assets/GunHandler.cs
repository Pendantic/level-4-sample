﻿using UnityEngine;
using System.Collections;

public class GunHandler : MonoBehaviour
{

	public Gun gun;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.R)) {
			gun.Reload ();
		}
		if (gun.isAutoFire) {
			if (Input.GetMouseButton (0)) {
				gun.Fire (Camera.main.transform);
			}
		} else {
			if (Input.GetMouseButtonDown (0)) {
				gun.Fire (Camera.main.transform);
			}
		}
	}
}
