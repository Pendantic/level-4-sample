﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AmmoDisplay : MonoBehaviour
{
	private Text label;
	private Gun playersGun;
	// Use this for initialization
	void Start ()
	{
		label = GetComponent<Text> ();
		playersGun = FindObjectOfType<PlayerMovement> ().GetComponentInChildren<Gun> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (playersGun.reloadTimeLeft > 0) {
			label.text = "Reloading";
		} else {
			label.text = playersGun.currentClip.ToString ();
		}
	}
}
