﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : Shootable
{
	public int maxHealth = 100;
	public float initialHealDelay = 2f;
	public float healRate = 0.2f;
	private float tillNextHeal = 0;

	public Image bloodOverlay;
	// Use this for initialization
	void Start ()
	{
		health = maxHealth;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (health < maxHealth) {
			tillNextHeal -= Time.deltaTime;
			if (tillNextHeal <= 0) {
				health++;
				tillNextHeal = healRate;
			}
		}
		Color c = bloodOverlay.color;
		c.a = 1 - ((float)health / (float)maxHealth);
		bloodOverlay.color = c;
	}
	public override void OnHit (int damage)
	{
		health -= damage;
		tillNextHeal = initialHealDelay;
		if (health <= 0) {
			Color c = bloodOverlay.color;
			c.a = 1;
			bloodOverlay.color = c;
			Destroy (GetComponent<PlayerMovement> ());
			Destroy (this);
			Rigidbody rb = GetComponent<Rigidbody> ();
			rb.constraints = RigidbodyConstraints.FreezeRotationY;
		}
	}
}
