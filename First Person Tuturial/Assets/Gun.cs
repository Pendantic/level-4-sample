﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{
	public GameObject hitEffect;

	public bool isAutoFire = true;

	public int clipSize = 5;
	public int currentClip = 0;

	public float reloadTime = 1.5f;
	public float reloadTimeLeft = 0;

	public float fireRate = 0.25f;
	private float tillNextFire = 0;

	public float inaccuracyAmount = 20f;

	public int damage = 2;
	void Start ()
	{
		currentClip = clipSize;
	}
	// Update is called once per frame
	void Update ()
	{
		tillNextFire -= Time.deltaTime;
		if (reloadTimeLeft > 0) {
			reloadTimeLeft -= Time.deltaTime;
			if (reloadTimeLeft <= 0) {
				currentClip = clipSize;
			} 
		}
	}
	public void Fire (Transform fireFrom)
	{
		if (isAutoFire && tillNextFire > 0) {
			return;
		}
		if (reloadTimeLeft > 0) {
			return;
		}
		if (currentClip > 0) {

			currentClip--;
			tillNextFire = fireRate;
			RaycastHit hitInfo;

			Vector2 inaccuracyDirection = Random.insideUnitCircle;
			Vector3 inaccuracy = fireFrom.TransformVector (inaccuracyDirection);
			Vector3 fireDirection = (fireFrom.forward + (inaccuracy * (inaccuracyAmount * 0.01f))).normalized;
			Ray ray = new Ray (fireFrom.position, fireDirection);
			if (Physics.Raycast (ray, out hitInfo)) {
				GameObject go = Instantiate (hitEffect);
				go.transform.position = hitInfo.point;
				go.transform.forward = hitInfo.normal;
				Shootable hit = hitInfo.collider.GetComponent<Shootable> ();
				if (hit != null) {
					hit.OnHit (damage);
				}
			}
		} else {
			Reload ();
		}
	}
	public void Reload ()
	{
		if (reloadTimeLeft <= 0) {
			reloadTimeLeft = reloadTime;
		}
	}
}
