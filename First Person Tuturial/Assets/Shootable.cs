﻿using UnityEngine;
using System.Collections;

public class Shootable : MonoBehaviour
{
	public int health = 5;
	public virtual void OnHit (int damage)
	{
		health -= damage;
		if (health <= 0) {
			Destroy (gameObject);
		}
	}
}
