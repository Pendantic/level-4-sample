﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	public float walkSpeed = 5;
	public float runSpeed = 10;
	public float jumpForce = 10;
	public float lookXSensitivity = 2;
	public float lookYSensitivity = 2;
	public float maxXRotation = 60;
	public float groundedDistance = 1.1f;
	public bool inverseXRotation = false;
	private Camera cam;
	private Rigidbody rb;
	private float currentRotation;
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		cam = GetComponentInChildren<Camera> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;


		float mouseX = Input.GetAxis ("Mouse X");
		transform.Rotate (0, mouseX * lookXSensitivity, 0);

		float mouseY = Input.GetAxis ("Mouse Y");
		if (inverseXRotation) {
			mouseY = -mouseY;
		}
		currentRotation -= mouseY * lookYSensitivity;
		currentRotation = Mathf.Clamp (currentRotation, -maxXRotation, maxXRotation);
		cam.transform.localEulerAngles = new Vector3 (currentRotation, 0, 0);

		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		Vector3 move = new Vector3 (horizontal, 0, vertical);
		if (move.magnitude > 1) {
			move = move.normalized;
		}
		move = transform.TransformVector (move);
		if (Input.GetKey (KeyCode.LeftShift)) {
			move *= runSpeed;
		} else {
			move *= walkSpeed;
		}
		if (Input.GetKeyDown (KeyCode.Space) && IsGrounded ()) {
			move.y = jumpForce;
		} else {
			move.y = rb.velocity.y;
		}

		rb.velocity = move;
	}
	bool IsGrounded ()
	{
		Ray ray = new Ray (transform.position, Vector3.down);
		Debug.DrawRay (transform.position, Vector3.down * groundedDistance, Color.red);
		return Physics.Raycast (ray, groundedDistance);
	}
}
